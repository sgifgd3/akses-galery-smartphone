﻿using System.Collections;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UploadImage : MonoBehaviour
{
    public Text debug;
    public Text debugUpload;
    public Image image;
    public InputField URL;

    private AccessGallery aksesGallery;
    private Texture2D loadedTexture;
    private string imagePath;
    private string imageExt;
    private Sprite tempSpriteCreate;

    private void Awake()
    {
        aksesGallery = GetComponent<AccessGallery>();
    }

    // Start is called before the first frame update
    void Start()
    {
        AccessGallery.OnImageLoad += OnImageLoad;
    }

    public void ButtonUploadImage()
    {
#if UNITY_EDITOR
        StartCoroutine(UploadTexture("http://localhost/UnityUploadImage/uploadImage2.php"));
#else
StartCoroutine(UploadTexture(URL.text));
#endif

    }

    public void ButtonDownloadImage()
    {
        StartCoroutine(DownloadTexture(URL.text));
    }

    public void ButtonPindahScene()
    {
        StartCoroutine(StartLoadScene());
    }

    private IEnumerator StartLoadScene()
    {
        image.sprite = null;
        Destroy(loadedTexture);
        Destroy(tempSpriteCreate);
        Debug.Log("["+Time.deltaTime+"] Unloading Unused Assets....");
        //menghapus asset yang sudah tidak di reference di memory
        yield return Resources.UnloadUnusedAssets().isDone;
        Debug.Log("["+Time.deltaTime+"] Unused Assets Unloaded.");
        //SceneManager.LoadScene("SampleScene");
    }

    private void OnImageLoad()
    {
        if (aksesGallery.GetTexture2D() == null)
        {
            Debug.Log("texture null");
            return;
        }

        loadedTexture = aksesGallery.GetTexture2D();

        imagePath = aksesGallery.imagePath;
        imageExt = Path.GetExtension(imagePath).ToLowerInvariant();
        debug.text = "Image Path: \n"+imagePath;
        tempSpriteCreate = Sprite.Create(loadedTexture, new Rect(0f, 0f, loadedTexture.width, loadedTexture.height), new Vector2(0.5f, 0.5f));
        image.sprite = tempSpriteCreate;
    }

    IEnumerator UploadTexture(string url) {

        WWWForm form = new WWWForm();
        byte[] textureBytes = null;

        Texture2D imageTexture = GetReadableTexture(loadedTexture);
        textureBytes = EncodedTexture(imageTexture);
        form.AddBinaryData("myImage", textureBytes, "imageFromUnity"+imageExt, "image/"+imageExt);

        UnityWebRequest uwr = UnityWebRequest.Post(url, form);

        yield return uwr.SendWebRequest();

        if (uwr.isHttpError || uwr.isNetworkError)
        {
            Debug.Log("error [ " + uwr.error + " ]");
            debugUpload.text = "Upload error [" + uwr.error + "]";
        }
        else
        {
            Debug.Log("Upload Complete");
            debugUpload.text = "Upload success \n"+url;
        }

        uwr.Dispose();
        Destroy(imageTexture);
    }

    IEnumerator DownloadTexture(string url)
    {
        UnityWebRequest uwr = new UnityWebRequest(url);
        DownloadHandlerTexture dlTexture = new DownloadHandlerTexture(true);
        uwr.downloadHandler = dlTexture;

        yield return uwr.SendWebRequest();

        if (uwr.isHttpError || uwr.isNetworkError)
        {
            Debug.Log("error [ " + uwr.error + " ]");
            debugUpload.text = "Download error [" + uwr.error + "]";
        }
        else
        {
            Debug.Log("Download Complete");
            debugUpload.text = "Download success \n" + url;
            loadedTexture = dlTexture.texture;
            tempSpriteCreate = Sprite.Create(loadedTexture, new Rect(0, 0, loadedTexture.width, loadedTexture.height), new Vector2(0.5f, 0.5f));
            image.sprite = tempSpriteCreate;
        }

        uwr.Dispose();
    }

    //makes texture readable
    private Texture2D GetReadableTexture(Texture2D source)
    {
        if (source == null)
        {
            Debug.Log("Texture2D null");
            debugUpload.text = "Texture2D null";
            return null;
        }

        //Create a RenderTexture
        RenderTexture rt = RenderTexture.GetTemporary(
            source.width,
            source.height,
            0,
            RenderTextureFormat.Default,
            RenderTextureReadWrite.Linear
            );

        //Copy source texture to the new render (RenderTexture)
        Graphics.Blit(source, rt);

        //Store the active RenderTexture & activate new created one (rt)
        RenderTexture previous = RenderTexture.active;
        RenderTexture.active = rt;

        //Create new Texture2D and fill its pixels from rt and apply changes
        Texture2D readableTexture = new Texture2D(source.width, source.height);
        readableTexture.ReadPixels(new Rect(0,0,rt.width,rt.height), 0,0);
        readableTexture.Apply();

        //activate the (previous) RenderTexture and release texture created with (GetTemporary() ...)
        RenderTexture.active = previous;
        RenderTexture.ReleaseTemporary(rt);

        return readableTexture;
    }

    private string GetImageExt(string path)//ambil image extension, jpg atau png
    {
        string[] split = path.Split('.');
        return path = split[split.Length - 1];
    }

    private byte[] EncodedTexture(Texture2D texture)
    {
        byte[] encodedTexture = null;

        switch (imageExt)
        {
            case ".jpg":
                encodedTexture = texture.EncodeToJPG();
                break;
            case ".png":
                encodedTexture = texture.EncodeToPNG();
                break;
            default:
                Debug.Log("Not readable extension");
                debugUpload.text = "Not readable extension";
                break;
        }

        return encodedTexture;
    } 
}
